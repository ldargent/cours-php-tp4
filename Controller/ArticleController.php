<?php

namespace Controller;

use Model\Manager\ArticleManager;
use View\View;

require_once __DIR__ . '/../Model/Manager/ArticleManager.php';
require_once  __DIR__ . '/../View/View.php';

/**
 * Class ArticleController
 * @package Controller
 */
class ArticleController
{
    /**
     * Article View action
     *
     * @throws \Exception
     */
    public function viewAction()
    {
        $articleId = (isset($_GET['id']) && !empty($_GET['id']))
            ? (int) $_GET['id']
            : null;

        if (is_null($articleId)) {
            throw new \Exception('Article id is missing');
        }

        $params = [ 'article' => ArticleManager::getById($articleId) ];

        View::render('article', $params);
    }
}