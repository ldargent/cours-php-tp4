<?php

namespace Controller;

use Model\Manager\ArticleManager;
use View\View;

require_once __DIR__ . '/../Model/Manager/ArticleManager.php';
require_once  __DIR__ . '/../View/View.php';

/**
 * Class HomeController
 * @package Controller
 */
class HomeController
{
    /**
     * Homepage View action
     *
     * @return mixed|void
     * @throws \Exception
     * @throws \View\Exceptions\ViewException
     */
    public function viewAction()
    {
        $params = [ 'articles' => ArticleManager::getAll() ];

        View::render('home', $params);
    }
}