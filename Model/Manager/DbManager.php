<?php

namespace Model\Manager;

/**
 * Database connection manager
 *
 * Class DbManager
 * @package Model\Manager
 */
abstract class DbManager
{
    /** @var \PDO */
    private static $db;

    /**
     * Initialize database connection and return it
     *
     * @return \PDO
     * @throws \Exception
     */
    public static function getDb()
    {
        // Connecting to the database if it's not done yet
        if (is_null(self::$db)) {
            $pdo = new \PDO("mysql:host=localhost;dbname=iut;charset=utf8", "ldargent", "iut@2019");
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            self::setDb($pdo);
        }

        return self::$db;
    }

    /**
     * Set database connection
     *
     * @param \PDO $db
     */
    private static function setDb($db)
    {
        self::$db = $db;
    }
}